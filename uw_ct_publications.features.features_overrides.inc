<?php
/**
 * @file
 * uw_ct_publications.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function uw_ct_publications_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: field_group
  $overrides["field_group.group_upload_file|node|uw_ct_person_profile|form.data|weight"] = 7;
  $overrides["field_group.group_upload_image|node|uw_ct_person_profile|form.data|weight"] = 6;

  // Exported overrides for: field_instance
  $overrides["field_instance.node-biblio-field_related_people.display|default|label"] = 'hidden';
  $overrides["field_instance.node-biblio-field_related_people.display|default|weight"] = 13;
  $overrides["field_instance.node-biblio-field_related_people.widget|weight"] = 77;
  $overrides["field_instance.node-uw_ct_person_profile-body.widget|weight"] = 5;
  $overrides["field_instance.node-uw_ct_person_profile-field_audience.display|default|weight"] = 16;
  $overrides["field_instance.node-uw_ct_person_profile-field_contact_information.widget|weight"] = 8;
  $overrides["field_instance.node-uw_ct_person_profile-field_file.display|default|weight"] = 13;
  $overrides["field_instance.node-uw_ct_person_profile-field_image.display|default|weight"] = 15;
  $overrides["field_instance.node-uw_ct_person_profile-field_profile_photo.display|default|weight"] = 14;
  $overrides["field_instance.node-uw_ct_person_profile-field_profile_type.description"] = 'Select specific profile type. For example, if you are currently a Ph.D. student select Ph.D. student.';
  $overrides["field_instance.node-uw_ct_person_profile-field_profile_type.display|default|module"] = 'taxonomy';
  $overrides["field_instance.node-uw_ct_person_profile-field_profile_type.display|default|type"] = 'taxonomy_term_reference_link';
  $overrides["field_instance.node-uw_ct_person_profile-field_profile_type.display|default|weight"] = 12;
  $overrides["field_instance.node-uw_ct_person_profile-field_profile_type.display|entity_teaser|weight"] = 0;
  $overrides["field_instance.node-uw_ct_person_profile-field_profile_type.display|forward"]["DELETED"] = TRUE;
  $overrides["field_instance.node-uw_ct_person_profile-field_profile_type.display|teaser|weight"] = 0;
  $overrides["field_instance.node-uw_ct_person_profile-field_profile_type.required"] = 0;
  $overrides["field_instance.node-uw_ct_person_profile-field_profile_type.settings|exclude_cv"]["DELETED"] = TRUE;
  $overrides["field_instance.node-uw_ct_person_profile-field_profile_type.widget|settings|label_help_description"] = '';
  $overrides["field_instance.node-uw_ct_person_profile-field_profile_type.widget|settings|select_parents"] = 0;
  $overrides["field_instance.node-uw_ct_person_profile-field_profile_type.widget|settings|start_minimized"] = 1;
  $overrides["field_instance.node-uw_ct_person_profile-field_profile_type.widget|weight"] = 18;
  $overrides["field_instance.node-uw_ct_person_profile-title_field.display|default|weight"] = 17;

  // Exported overrides for: node
  $overrides["node.biblio.description"] = 'Use Bibliography for scholarly content, such as journal papers and books.';
  $overrides["node.biblio.has_title"] = 1;
  $overrides["node.biblio.help"] = '';
  $overrides["node.biblio.name"] = 'Bibliography';
  $overrides["node.biblio.title_label"] = 'Title';

  // Exported overrides for: variable
  $overrides["variable.biblio_author_link_profile.value"] = 1;
  $overrides["variable.biblio_author_link_profile_path.value"] = '';
  $overrides["variable.biblio_download_links_to_node.value"] = 1;
  $overrides["variable.biblio_export_links.value|ris"] = 'ris';
  $overrides["variable.biblio_export_links.value|xml"] = 'xml';
  $overrides["variable.biblio_lookup_links.value|crossref"] = 'crossref';
  $overrides["variable.biblio_rowsperpage.value"] = '';
  $overrides["variable.biblio_search.value"] = 1;
  $overrides["variable.biblio_settings__active_tab.value"] = 'edit-links';
  $overrides["variable.field_bundle_settings_node__uw_ct_person_profile.value|extra_fields|display|author_publications_entity_view_1"] = array(
    'default' => array(
      'weight' => 7,
      'visible' => TRUE,
    ),
  );
  $overrides["variable.field_bundle_settings_node__uw_ct_person_profile.value|extra_fields|display|author_publications_entity_view_2"] = array(
    'default' => array(
      'weight' => 8,
      'visible' => TRUE,
    ),
  );
  $overrides["variable.field_bundle_settings_node__uw_ct_person_profile.value|extra_fields|display|author_publications_entity_view_3"] = array(
    'default' => array(
      'weight' => 6,
      'visible' => TRUE,
    ),
  );
  $overrides["variable.field_bundle_settings_node__uw_ct_person_profile.value|extra_fields|display|author_publications_entity_view_4"] = array(
    'default' => array(
      'weight' => 9,
      'visible' => TRUE,
    ),
  );
  $overrides["variable.field_bundle_settings_node__uw_ct_person_profile.value|extra_fields|display|author_publications_entity_view_5"] = array(
    'default' => array(
      'weight' => 10,
      'visible' => TRUE,
    ),
  );
  $overrides["variable.field_bundle_settings_node__uw_ct_person_profile.value|extra_fields|display|profile_publications_entity_view_1"] = array(
    'default' => array(
      'weight' => 6,
      'visible' => TRUE,
    ),
  );
  $overrides["variable.field_bundle_settings_node__uw_ct_person_profile.value|extra_fields|display|profile_publications_entity_view_2"] = array(
    'default' => array(
      'weight' => 7,
      'visible' => TRUE,
    ),
  );
  $overrides["variable.field_bundle_settings_node__uw_ct_person_profile.value|extra_fields|display|profile_publications_entity_view_3"] = array(
    'default' => array(
      'weight' => 8,
      'visible' => TRUE,
    ),
  );
  $overrides["variable.field_bundle_settings_node__uw_ct_person_profile.value|extra_fields|display|profile_publications_entity_view_4"] = array(
    'default' => array(
      'weight' => 9,
      'visible' => TRUE,
    ),
  );
  $overrides["variable.field_bundle_settings_node__uw_ct_person_profile.value|extra_fields|display|profile_publications_entity_view_5"] = array(
    'default' => array(
      'weight' => 10,
      'visible' => TRUE,
    ),
  );
  $overrides["variable.field_bundle_settings_node__uw_ct_person_profile.value|extra_fields|display|profile_publications_entity_view_6"] = array(
    'default' => array(
      'weight' => 11,
      'visible' => TRUE,
    ),
  );
  $overrides["variable.field_bundle_settings_node__uw_ct_person_profile.value|extra_fields|form|metatags"] = array(
    'weight' => 15,
  );
  $overrides["variable.field_bundle_settings_node__uw_ct_person_profile.value|view_modes|embedded"] = array(
    'custom_settings' => TRUE,
  );
  $overrides["variable.forward_display_uw_ct_person_profile.value"] = 0;
  $overrides["variable.uw_page_settings_node_node_type_biblio.value"] = 1;
  $overrides["variable.uw_page_settings_node_node_type_uw_ct_person_profile.value"] = 1;

 return $overrides;
}
