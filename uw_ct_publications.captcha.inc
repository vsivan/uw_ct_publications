<?php
/**
 * @file
 * uw_ct_publications.captcha.inc
 */

/**
 * Implements hook_captcha_default_points().
 */
function uw_ct_publications_captcha_default_points() {
  $export = array();

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_biblio_form';
  $captcha->module = '';
  $captcha->captcha_type = 'default';
  $export['comment_node_biblio_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_uw_ct_person_profile_form';
  $captcha->module = '';
  $captcha->captcha_type = 'default';
  $export['comment_node_uw_ct_person_profile_form'] = $captcha;

  return $export;
}
