<?php
/**
 * @file
 * uw_ct_publications.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_publications_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'add terms in profile_status'.
  $permissions['add terms in profile_status'] = array(
    'name' => 'add terms in profile_status',
    'roles' => array(),
    'module' => 'taxonomy_access_fix',
  );

  // Exported permission: 'add terms in vip_profile_types'.
  $permissions['add terms in vip_profile_types'] = array(
    'name' => 'add terms in vip_profile_types',
    'roles' => array(),
    'module' => 'taxonomy_access_fix',
  );

  // Exported permission: 'delete terms in profile_status'.
  $permissions['delete terms in profile_status'] = array(
    'name' => 'delete terms in profile_status',
    'roles' => array(),
    'module' => 'taxonomy',
  );

  // Exported permission: 'delete terms in vip_profile_types'.
  $permissions['delete terms in vip_profile_types'] = array(
    'name' => 'delete terms in vip_profile_types',
    'roles' => array(),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in profile_status'.
  $permissions['edit terms in profile_status'] = array(
    'name' => 'edit terms in profile_status',
    'roles' => array(),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in vip_profile_types'.
  $permissions['edit terms in vip_profile_types'] = array(
    'name' => 'edit terms in vip_profile_types',
    'roles' => array(),
    'module' => 'taxonomy',
  );

  // Exported permission: 'search uw_ct_person_profile content'.
  $permissions['search uw_ct_person_profile content'] = array(
    'name' => 'search uw_ct_person_profile content',
    'roles' => array(),
    'module' => 'search_config',
  );

  return $permissions;
}
