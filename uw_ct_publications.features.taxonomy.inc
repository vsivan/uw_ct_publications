<?php
/**
 * @file
 * uw_ct_publications.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function uw_ct_publications_taxonomy_default_vocabularies() {
  return array(
    'vip_profile_types' => array(
      'name' => 'VIP Profile Types',
      'machine_name' => 'vip_profile_types',
      'description' => 'Profile Types at the VIP Lab',
      'hierarchy' => 1,
      'module' => 'taxonomy',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
