<?php
/**
 * @file
 * uw_ct_publications.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function uw_ct_publications_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_people-profiles:people-profiles.
  $menu_links['main-menu_people-profiles:people-profiles'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'people-profiles',
    'router_path' => 'people-profiles',
    'link_title' => 'People profiles',
    'options' => array(
      'alter' => TRUE,
      'identifier' => 'main-menu_people-profiles:people-profiles',
    ),
    'module' => 'system',
    'hidden' => 1,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
    'parent_identifier' => 'main-menu_about-vision-and-image-processing-lab:node/2',
  );
  // Exported menu link: management_wysiwyg-profiles:admin/config/content/wysiwyg.
  $menu_links['management_wysiwyg-profiles:admin/config/content/wysiwyg'] = array(
    'menu_name' => 'management',
    'link_path' => 'admin/config/content/wysiwyg',
    'router_path' => 'admin/config/content/wysiwyg',
    'link_title' => 'Wysiwyg profiles',
    'options' => array(
      'attributes' => array(
        'title' => 'Configure client-side editors.',
      ),
      'alter' => TRUE,
      'identifier' => 'management_wysiwyg-profiles:admin/config/content/wysiwyg',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'language' => 'und',
    'menu_links_customized' => 0,
    'parent_identifier' => 'management_content-authoring:admin/config/content',
  );
  // Exported menu link: navigation_person-profile:node/add/uw-ct-person-profile.
  $menu_links['navigation_person-profile:node/add/uw-ct-person-profile'] = array(
    'menu_name' => 'navigation',
    'link_path' => 'node/add/uw-ct-person-profile',
    'router_path' => 'node/add/uw-ct-person-profile',
    'link_title' => 'Person Profile',
    'options' => array(
      'attributes' => array(
        'title' => 'A person profile, which is optionally promoted to the front page sidebar and appears under the "Person Profiles" section of your site.',
      ),
      'identifier' => 'navigation_person-profile:node/add/uw-ct-person-profile',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'language' => 'und',
    'menu_links_customized' => 0,
    'parent_identifier' => 'navigation_add-content:node/add',
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('People profiles');
  t('Person Profile');
  t('Wysiwyg profiles');

  return $menu_links;
}
