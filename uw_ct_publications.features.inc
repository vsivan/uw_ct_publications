<?php
/**
 * @file
 * uw_ct_publications.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_ct_publications_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "captcha" && $api == "captcha") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uw_ct_publications_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_field_group_info_alter().
 */
function uw_ct_publications_field_group_info_alter(&$data) {
  if (isset($data['group_upload_file|node|uw_ct_person_profile|form'])) {
    $data['group_upload_file|node|uw_ct_person_profile|form']->data['weight'] = 7; /* WAS: 6 */
  }
  if (isset($data['group_upload_image|node|uw_ct_person_profile|form'])) {
    $data['group_upload_image|node|uw_ct_person_profile|form']->data['weight'] = 6; /* WAS: 5 */
  }
}

/**
 * Implements hook_field_default_field_instances_alter().
 */
function uw_ct_publications_field_default_field_instances_alter(&$data) {
  if (isset($data['node-biblio-field_related_people'])) {
    $data['node-biblio-field_related_people']['display']['default']['label'] = 'hidden'; /* WAS: 'above' */
    $data['node-biblio-field_related_people']['display']['default']['weight'] = 13; /* WAS: 3 */
    $data['node-biblio-field_related_people']['widget']['weight'] = 77; /* WAS: 63 */
  }
  if (isset($data['node-uw_ct_person_profile-body'])) {
    $data['node-uw_ct_person_profile-body']['widget']['weight'] = 5; /* WAS: 4 */
  }
  if (isset($data['node-uw_ct_person_profile-field_audience'])) {
    $data['node-uw_ct_person_profile-field_audience']['display']['default']['weight'] = 16; /* WAS: 8 */
  }
  if (isset($data['node-uw_ct_person_profile-field_contact_information'])) {
    $data['node-uw_ct_person_profile-field_contact_information']['widget']['weight'] = 8; /* WAS: 7 */
  }
  if (isset($data['node-uw_ct_person_profile-field_file'])) {
    $data['node-uw_ct_person_profile-field_file']['display']['default']['weight'] = 13; /* WAS: 4 */
  }
  if (isset($data['node-uw_ct_person_profile-field_image'])) {
    $data['node-uw_ct_person_profile-field_image']['display']['default']['weight'] = 15; /* WAS: 7 */
  }
  if (isset($data['node-uw_ct_person_profile-field_profile_photo'])) {
    $data['node-uw_ct_person_profile-field_profile_photo']['display']['default']['weight'] = 14; /* WAS: 6 */
  }
  if (isset($data['node-uw_ct_person_profile-field_profile_type'])) {
    $data['node-uw_ct_person_profile-field_profile_type']['description'] = 'Select specific profile type. For example, if you are currently a Ph.D. student select Ph.D. student.'; /* WAS: '' */
    $data['node-uw_ct_person_profile-field_profile_type']['display']['default']['module'] = 'taxonomy'; /* WAS: '' */
    $data['node-uw_ct_person_profile-field_profile_type']['display']['default']['type'] = 'taxonomy_term_reference_link'; /* WAS: 'hidden' */
    $data['node-uw_ct_person_profile-field_profile_type']['display']['default']['weight'] = 12; /* WAS: 5 */
    $data['node-uw_ct_person_profile-field_profile_type']['display']['entity_teaser']['weight'] = 0; /* WAS: 3 */
    $data['node-uw_ct_person_profile-field_profile_type']['display']['teaser']['weight'] = 0; /* WAS: 3 */
    $data['node-uw_ct_person_profile-field_profile_type']['required'] = 0; /* WAS: 1 */
    $data['node-uw_ct_person_profile-field_profile_type']['widget']['settings']['label_help_description'] = ''; /* WAS: '' */
    $data['node-uw_ct_person_profile-field_profile_type']['widget']['settings']['select_parents'] = 0; /* WAS: 1 */
    $data['node-uw_ct_person_profile-field_profile_type']['widget']['settings']['start_minimized'] = 1; /* WAS: 0 */
    $data['node-uw_ct_person_profile-field_profile_type']['widget']['weight'] = 18; /* WAS: 8 */
    unset($data['node-uw_ct_person_profile-field_profile_type']['display']['forward']);
    unset($data['node-uw_ct_person_profile-field_profile_type']['settings']['exclude_cv']);
  }
  if (isset($data['node-uw_ct_person_profile-title_field'])) {
    $data['node-uw_ct_person_profile-title_field']['display']['default']['weight'] = 17; /* WAS: 9 */
  }
}

/**
 * Implements hook_node_info_alter().
 */
function uw_ct_publications_node_info_alter(&$data) {
  if (isset($data['biblio'])) {
    $data['biblio']['description'] = 'Use Bibliography for scholarly content, such as journal papers and books.'; /* WAS: 'Use Biblio for scholarly content, such as journal papers and books.' */
    $data['biblio']['has_title'] = 1; /* WAS: '' */
    $data['biblio']['help'] = ''; /* WAS: '' */
    $data['biblio']['name'] = 'Bibliography'; /* WAS: 'Biblio' */
    $data['biblio']['title_label'] = 'Title'; /* WAS: '' */
  }
}

/**
 * Implements hook_strongarm_alter().
 */
function uw_ct_publications_strongarm_alter(&$data) {
  if (isset($data['biblio_author_link_profile'])) {
    $data['biblio_author_link_profile']->value = 1; /* WAS: 0 */
  }
  if (isset($data['biblio_author_link_profile_path'])) {
    $data['biblio_author_link_profile_path']->value = ''; /* WAS: 'user/[user:uid]' */
  }
  if (isset($data['biblio_download_links_to_node'])) {
    $data['biblio_download_links_to_node']->value = 1; /* WAS: 0 */
  }
  if (isset($data['biblio_export_links'])) {
    $data['biblio_export_links']->value['ris'] = 'ris'; /* WAS: 0 */
    $data['biblio_export_links']->value['xml'] = 'xml'; /* WAS: 0 */
  }
  if (isset($data['biblio_lookup_links'])) {
    $data['biblio_lookup_links']->value['crossref'] = 'crossref'; /* WAS: 0 */
  }
  if (isset($data['biblio_rowsperpage'])) {
    $data['biblio_rowsperpage']->value = ''; /* WAS: 99999 */
  }
  if (isset($data['biblio_search'])) {
    $data['biblio_search']->value = 1; /* WAS: 0 */
  }
  if (isset($data['biblio_settings__active_tab'])) {
    $data['biblio_settings__active_tab']->value = 'edit-links'; /* WAS: 'edit-keywords' */
  }
  if (isset($data['field_bundle_settings_node__uw_ct_person_profile'])) {
    $data['field_bundle_settings_node__uw_ct_person_profile']->value['extra_fields']['display']['author_publications_entity_view_1'] = array(
      'default' => array(
        'weight' => 7,
        'visible' => TRUE,
      ),
    ); /* WAS: '' */
    $data['field_bundle_settings_node__uw_ct_person_profile']->value['extra_fields']['display']['author_publications_entity_view_2'] = array(
      'default' => array(
        'weight' => 8,
        'visible' => TRUE,
      ),
    ); /* WAS: '' */
    $data['field_bundle_settings_node__uw_ct_person_profile']->value['extra_fields']['display']['author_publications_entity_view_3'] = array(
      'default' => array(
        'weight' => 6,
        'visible' => TRUE,
      ),
    ); /* WAS: '' */
    $data['field_bundle_settings_node__uw_ct_person_profile']->value['extra_fields']['display']['author_publications_entity_view_4'] = array(
      'default' => array(
        'weight' => 9,
        'visible' => TRUE,
      ),
    ); /* WAS: '' */
    $data['field_bundle_settings_node__uw_ct_person_profile']->value['extra_fields']['display']['author_publications_entity_view_5'] = array(
      'default' => array(
        'weight' => 10,
        'visible' => TRUE,
      ),
    ); /* WAS: '' */
    $data['field_bundle_settings_node__uw_ct_person_profile']->value['extra_fields']['display']['profile_publications_entity_view_1'] = array(
      'default' => array(
        'weight' => 6,
        'visible' => TRUE,
      ),
    ); /* WAS: '' */
    $data['field_bundle_settings_node__uw_ct_person_profile']->value['extra_fields']['display']['profile_publications_entity_view_2'] = array(
      'default' => array(
        'weight' => 7,
        'visible' => TRUE,
      ),
    ); /* WAS: '' */
    $data['field_bundle_settings_node__uw_ct_person_profile']->value['extra_fields']['display']['profile_publications_entity_view_3'] = array(
      'default' => array(
        'weight' => 8,
        'visible' => TRUE,
      ),
    ); /* WAS: '' */
    $data['field_bundle_settings_node__uw_ct_person_profile']->value['extra_fields']['display']['profile_publications_entity_view_4'] = array(
      'default' => array(
        'weight' => 9,
        'visible' => TRUE,
      ),
    ); /* WAS: '' */
    $data['field_bundle_settings_node__uw_ct_person_profile']->value['extra_fields']['display']['profile_publications_entity_view_5'] = array(
      'default' => array(
        'weight' => 10,
        'visible' => TRUE,
      ),
    ); /* WAS: '' */
    $data['field_bundle_settings_node__uw_ct_person_profile']->value['extra_fields']['display']['profile_publications_entity_view_6'] = array(
      'default' => array(
        'weight' => 11,
        'visible' => TRUE,
      ),
    ); /* WAS: '' */
    $data['field_bundle_settings_node__uw_ct_person_profile']->value['extra_fields']['form']['metatags'] = array(
      'weight' => 15,
    ); /* WAS: '' */
    $data['field_bundle_settings_node__uw_ct_person_profile']->value['view_modes']['embedded'] = array(
      'custom_settings' => TRUE,
    ); /* WAS: '' */
  }
  if (isset($data['forward_display_uw_ct_person_profile'])) {
    $data['forward_display_uw_ct_person_profile']->value = 0; /* WAS: FALSE */
  }
  if (isset($data['uw_page_settings_node_node_type_biblio'])) {
    $data['uw_page_settings_node_node_type_biblio']->value = 1; /* WAS: 0 */
  }
  if (isset($data['uw_page_settings_node_node_type_uw_ct_person_profile'])) {
    $data['uw_page_settings_node_node_type_uw_ct_person_profile']->value = 1; /* WAS: 0 */
  }
}
